import os
import json
from requests import get
from requests.utils import quote
from lxml import html, etree
from lxml.html import clean
from time import sleep

onemg_url_base = 'https://www.1mg.com/search/all?name=%s'
xpath_drug_name = '//div[@class="row search-sku-item"]//div[@class="col-xs-7 col-sm-9 js-sku-info"]/a/@data-selected-product'
xpath_drug_price = '//div[@class="row search-sku-item"]//div[@class="text-small search-sku-price"]/text()'
xpath_drug_quantity = '//div[@class="row search-sku-item"]//div[@class="text-xsmall text-black mt"]/text()'

def get_new_jsons():
    j_files = sorted (os.listdir('./data/'))
    for j_file in j_files:
        with open('./data/%s' % j_file) as fp:
            j_data = json.load(fp)
        j_drugs = j_data['Drugs']
        print 'Company : %s (%d, %d) ; Total Drugs : %s' % ( j_data['Company Name'], j_files.index(j_file), len(j_files) - j_files.index(j_file), len(j_drugs))
        for index in range(len(j_drugs)):
            j_drug = j_drugs[index]
            if 'Pricing' in j_drug:
                continue
            drug_name = j_drug['Drug Name']
            onemg_url = onemg_url_base % quote(drug_name, safe='')
            page = get(onemg_url)
            new_j = get_pricing(page)
            print '    Drug %d (%d): %s' % ( index , index - len(j_drugs) ,  drug_name)
            print new_j
            j_data['Drugs'][index]['Pricing'] = new_j
            with open('./data/%s' % j_file, 'w') as fp:
                json.dump(j_data, fp)
            sleep(1)

def get_pricing(page):
    tree = html.fromstring(page.text)
    drug_names = tree.xpath(xpath_drug_name)
    drug_prices = tree.xpath(xpath_drug_price)
    drug_quantities = tree.xpath(xpath_drug_quantity)
    j_result = []
    if len(drug_names) == 0:
        return j_result
    for index in range(len(drug_names)):
        j_each = {}
        j_each['Name'] = drug_names[index]
        j_each['Price'] = drug_prices[index]
        j_each['Quantity'] = drug_quantities[index]
        j_result.append(j_each)
    return j_result

if __name__ == '__main__':
    get_new_jsons()
